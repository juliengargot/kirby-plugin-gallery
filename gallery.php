<?php
/**
 * Gallery Plugin
 *
 * @author Julien Gargot <julien@g-u-i.me>
 * @version 1.0.0
 */
kirbytext::$pre[] = function($kirbytext, $text) {
  $text = preg_replace_callback('!\(gallery(…|\.{3})\)(.*?)\((…|\.{3})gallery\)!is', function($matches) use($kirbytext) {
    $html = $matches[2];
    $images = preg_match_all('/\(image:([^\)]+)\)/i', $html);
    if ($images%2==0 && $images<5) {
      $grid_size = 2;
    }
    else if ($images%3==0 && $images<7) {
      $grid_size = 3;
    }
    else if ($images<9) {
      $grid_size = 4;
    }
    else {
      $grid_size = 5;
    }
    return '<div class="' . c::get('gallery.container', 'gallery') . '" data-count-images="' . $images . '" data-grid="' . $grid_size . '">' . kirbytext($html) . '</div>';
  }, $text);
  return $text;
};
